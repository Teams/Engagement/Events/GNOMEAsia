GNOME.Asia Summit 2018 Travel/Venue Information (Update date : 4/11)
====

## Conference Position

* Chinese Name : 臺灣科技大學
* English Name : National Taiwan University of Science and Technology (NTUST)

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28252.044277502802!2d121.53935128488158!3d25.01296296676957!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442aa2176c4c0ad%3A0x90db5e44ee29f455!2sNational+Taiwan+University+of+Science+and+Technology!5e0!3m2!1sen!2stw!4v1523455127999" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>




# Accomadation
:::info
* If traveler want to book room for yourself, the price is about 1000 - 1200 NTD/per night.

* If traveler would like to share room with other, the price is about 800 - 1000 NTD/per night.

* If traveler would like to sleep with strangers, the price is about 800 - 1000 NTD/per night. 
:::

## Hostels (For backpackers)

### May Rooms Taipei NTU
 
* Room type & Price 
    * Private Rooms : 1600/per night,per room 
    * Beds in Dorm Rooms : 600/per night, per bed

* [Information Link](https://www.booking.com/hotel/tw/may-rooms-taipei-ntu-tai-bei-shi.html?label=gen173nr-1DCAEoggJCAlhYSDNYBGjnAYgBAZgBMLgBB8gBDNgBA-gBAZICAXmoAgM;sid=bdb39c241eb5e464353160012ec3fd94;all_sr_blocks=298752506_110568305_1_1_0;bshb=0;checkin=2018-08-09;checkout=2018-08-15;dest_id=-2637882;dest_type=city;dist=0;group_adults=1;group_children=0;hapos=1;highlighted_blocks=298752506_110568305_1_1_0;hpos=1;no_rooms=1;room1=A;sb_price_type=total;srepoch=1523455317;srfid=efcaa30bef77b446e580192c79c8b22ae8f5e325X1;srpvid=101f62a9eeda0167;type=total;ucfs=1&#hotelTmpl)

* Location (Very close to NTUST)
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15661.886958017887!2d121.53089145398478!3d25.01284342093483!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442aa1fe1eae12f%3A0x113afefd7d8120a5!2sMay+Rooms+Taipei+NTU!5e0!3m2!1sen!2stw!4v1523455956008" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

### Ximen Corner Hostel

* Room type & Price 
    * Private Rooms: 2000/per night, per room
    * Beds in Dorm Rooms : 600/per night, per bed


* [Information Link](https://www.booking.com/hotel/tw/here-there-hostel.html?aid=304142;label=gen173nr-1DCAEoggJCAlhYSDNYBGjnAYgBAZgBMLgBB8gBDNgBA-gBAZICAXmoAgM;sid=bdb39c241eb5e464353160012ec3fd94;atlas_src=sr_iw_btn&checkin=2018-08-09&checkout=2018-08-15&group_adults=1&group_children=0&nflt=pri%3D1%3Brshl%3D7%3Brshl%3D8%3Brshl%3D9%3Bht_id%3D203%3B&no_rooms=1&room1=A&sb_price_type=total&srepoch=1523456196&srpvid=a9c66443e91a0068&type=total&)


* Location 
    * MRT : Twenty minutes from ximen Station to Gongguan Station

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.7313967825007!2d121.50460631485717!3d25.04318798396805!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442a9091399fd85%3A0x24e4374a9fc241fc!2z6KW_6ZaA6KGX6KeS5peF5bqXIFhpbWVuIENvcm5lciBIb3N0ZWw!5e0!3m2!1szh-TW!2stw!4v1523457475418" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

## Hotels

### ColorMix Hotel & Hostel

* Room type & Price 
    * Single Room with Shared Bathroom : 1200 NTD

* [Information Link](https://www.booking.com/hotel/tw/colormix.html?aid=304142;label=gen173nr-1DCAEoggJCAlhYSDNYBGjnAYgBAZgBMLgBB8gBDNgBA-gBAZICAXmoAgM;sid=bdb39c241eb5e464353160012ec3fd94;atlas_src=sr_iw_btn&checkin=2018-08-09&checkout=2018-08-15&group_adults=1&group_children=0&nflt=pri%3D1%3Bht_id%3D204%3Boos%3D1%3Brshl%3D9%3Breview_score%3D70%3Breview_score%3D80%3B&no_rooms=1&room1=A&sb_price_type=total&srepoch=1523458198&srpvid=9bcb66f2c23e0277&type=total&)


* Location 
    * MRT : Twenty minutes from ximen Station to Gongguan Station

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.750792267201!2d121.5030403148572!3d25.042529983968375!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442a908515f1c73%3A0x64dc52acaaf2fafb!2z5Y2h5qiC55S65peF5bqXIENvbG9ybWl4IEhvdGVsICYgSG9zdGVs!5e0!3m2!1szh-TW!2stw!4v1523458424114" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

### NYS Loft Hotel

* Room type & Price 
    * Standard Single Room with Shared Bathroom : 1200 NTD

* [Information Link](https://www.booking.com/hotel/tw/nan-yang-jie-yi-hao-lu-dian.html?aid=304142;label=gen173nr-1DCAEoggJCAlhYSDNYBGjnAYgBAZgBMLgBB8gBDNgBA-gBAZICAXmoAgM;sid=bdb39c241eb5e464353160012ec3fd94;atlas_src=sr_iw_btn&checkin=2018-08-09&checkout=2018-08-15&group_adults=1&group_children=0&nflt=pri%3D1%3Bht_id%3D204%3Boos%3D1%3Brshl%3D9%3Breview_score%3D70%3Breview_score%3D80%3B&no_rooms=1&room1=A&sb_price_type=total&srepoch=1523458442&srpvid=9bcb66f2c23e0277&type=total&)

* Location 
    * MRT : Thirty minutes from Taipei Main Station to Gongguan Station
 
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3614.6560505288808!2d121.51399331485726!3d25.04574398396681!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442a972e2932cdd%3A0x9388230e6e45c75c!2zTllTIExPRlQgSE9URUwg5Y2X6Zm96KGX5aO56Jmf!5e0!3m2!1szh-TW!2stw!4v1523459369495" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
 

 
# MRT Station Name

## Red Line
* 淡水   Danshui  
* 紅樹林   Hongshulin  
* 竹圍   Zhuwei  
* 關渡   Guandu  
* 忠義   Zhongyi  
* 復興崗   Fuxinggang  
* 北投   Beitou  
* 新北投   Xinbeitou  
* 奇岩   Qiyan  
* 口其哩岸   Qilian  
* 石牌   Shipai  
* 明德   Mingde  
* 芝山   Zhishan  
* 士林   Shilin  
* 劍潭   Jiantan  
* 圓山   Yuanshan  
* 民權西路   Minquan W.Rd.  
* 雙連   Shuanglian  
* 中山   Zhongshan  
* 台北車站   Taipei Main Station  
* 臺大醫院   NTU Hospital  
* 中正紀念堂   Chiang Kai-Shek Memorial Hall  
* 古亭   Guting

  
## Green Line
* 台電大樓   Taipower Building  
* 公館   Gongguan  
* 萬隆   Wanlong  
* 景美   Jingmei  
* 大坪林   Dapinglin  
* 七張   Qizhang  
* 小碧潭   Xiaobitan  
* 新店市公所   Xindan City Hall  
* 新店   Xindan  
  

## Yellow Line
* 頂溪   Dingxi 
* 永安市場   Yongan Market  
* 景安   Jingan  
* 南勢角   Nanshijiao  
* Blue Line  
* 永寧   Yongning  
* 土城   Tucheng  
* 海山   Haishan  
* 亞東醫院   Far Eastern Hospital  
* 府中   Fuzhong  
* 板橋   Banciao  
* 新埔   Xinpu  
* 江子翠   Jiangzicui  
* 龍山寺   Longshan Temple  
* 西門   Ximen  
* 小南門   Xiaonanmen  
* 善導寺   Shandao Temple  
* 忠孝新生   Zhongxiao Xinsheng  
* 忠孝復興   Zhongxiao Fuxing  
* 忠孝敦化   Zhongxiao Dunhua  
* 國父紀念館   Sun Yat-Sen Memorial Hall  
* 市政府   Taipei City Hall  
* 永春   Yongchun  
* 後山碑   Houshanpi  
* 昆陽   Kunyang  
  

## Brown Line
* 中山國中   Zhongshan Jr. High School  
* 南京東路   Nanjing E Rd.  
* 大安   Daan  
* 科技大樓   Technology Building  
* 六張犁   Liuzhangli  
* 麟光   Linguang  
* 辛亥   Xinhai  
* 萬芳醫院   Wangfang Hospital  
* 萬芳社區   Wangfang Community  
* 木柵   Muzha  
* 動物園   Taipei Zoo
