= May 08 2018 Meeting =

Attendees: 
 1. haggen
 1. muelli
 1. sammyfung
 1. nuritzi
 1. sakana
 1. wenqixiang
 1. BinLi
 1. dhsung
 1. tommy

== Agenda ==

 * GNOME Asia Summit 2018 status
 * When will we meet next time?

== Meeting notes ==

 * GNOME.Asia Summit 2018 status
   * GNOME.Asia Summit 2018 logo was voted, No.1 masterpiece is the winner.
    * Logo links: https://gitlab.gnome.org/Community/Engagement/GNOMEAsia/uploads/d730a803a1e4506f5e5237c3c7cfe72f/gnome-asia-summit-logo-01.png
   * GNOME.Asia Website was online, http://2018.gnome.asia .
   * Need to spread CFP in more social media.
   * Will invite a member to Taipei for keynote after Board election.

 * When will we meet next?
   * May 22 2018

== Action items ==

 * nuritzi will invite Neil to join the next meeting.
 * sakana need confirm with local team about the sponsorship package from GUADEC brochure.

